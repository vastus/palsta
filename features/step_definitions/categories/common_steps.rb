Given(/^there are categories "(.*?)"$/) do |names|
  names.split(", ").each do |category_name|
    create(:category, :name => category_name)
  end
end

