#
# Presumer -- Exceptional
#

Then(/^user sees "([^"]*?)"$/) do |text|
  expect(page).to have_content(text)
end

Then(/^user sees "(.*?)" within "(.*?)"$/) do |text, selector|
  within selector do
    expect(page).to have_content(text)
  end
end

