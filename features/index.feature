Feature: Index

  Scenario: Shows all categories
    Given there are categories "General, Photography, Light"
    When user is on the "home" page
    Then user sees "General" within ".categories"
    And user sees "Photography" within ".categories"
    And user sees "Light" within ".categories"

