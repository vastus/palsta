module Helpers
  def visit_page(page)
    case page
    when "home"
      visit(root_path)
    else
      raise "Path not set"
    end
  end
end

