class CreateCategories < ActiveRecord::Migration
  def change
    create_table :categories do |t|
      t.string(:name, :unique => true, :null => false, :default => "Unnamed")

      t.timestamps null: false
    end
  end
end

