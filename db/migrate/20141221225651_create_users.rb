class CreateUsers < ActiveRecord::Migration
  def change
    create_table :users do |t|
      t.string :provider, :null => false
      t.string :uid, :null => false
      t.hstore :auth, :null => false, :default => {}

      t.timestamps null: false
    end

    # http://blog.arkency.com/2014/10/how-to-persist-hashes-in-rails-applications-with-postgresql/#awesomeness
    add_index :users, :auth, :name => "users_auth_gin_index", :using => :gin
  end
end

